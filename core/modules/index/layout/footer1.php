<?php
	echo '
		<footer id="footer"><!--Footer-->
		<div class="footer-top">
			<div class="container">
				<div class="row-fluid">
					
					<div class="col-sm-12">
						<div class="col-sm-2">
							<div class="companyinfo">
									<h2><span>RED</span>-IDi</h2>
									<p>Investigación, Desarrollo e Innovación</p>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="img/home/iframe6.jpg" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-link"></i>
									</div>
								</a>
								<p>EDUNI</p>
								<h2>Info Adicional</h2>
							</div>
						</div>
						
						<div class="col-sm-2">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="img/home/iframe6.jpg" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-link"></i>
									</div>
								</a>
								<p>Fondo Editorial PUCP</p>
								<h2>Info Adicional</h2>
							</div>
						</div>
						
						<div class="col-sm-2">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="img/home/iframe6.jpg" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-link"></i>
									</div>
								</a>
								<p>Centro Editorial UPCH</p>
								<h2>Info Adicional</h2>
							</div>
						</div>
						
						<div class="col-sm-2">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="img/home/iframe6.jpg" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-link"></i>
									</div>
								</a>
								<p>Fondo Editorial UNMSM</p>
								<h2>Info Adicional</h2>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="img/home/iframe6.jpg" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-link"></i>
									</div>
								</a>
								<p>Fondo Editorial UNALM</p>
								<h2>Info Adicional</h2>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<!--
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Universidades</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">UNI</a></li>
								<li><a href="#">PUCP</a></li>
								<li><a href="#">UPCH</a></li>
								<li><a href="#">UNSM</a></li>
								<li><a href="#">UNALM</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Legal</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Terminos de Uso</a></li>
								<li><a href="#">Política de Privacidad</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Información</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Nosotros</a></li>
								<li><a href="#">Puntos de Venta</a></li>
								<li><a href="#">Normas de Publicación</a></li>
								
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Adicional</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Subitem 1</a></li>
								<li><a href="#">Subitem 2</a></li>
								<li><a href="#">Subitem 3</a></li>
								
							</ul>
						</div>
					</div>
					<div class="col-sm-3 col-sm-offset-1">
						<div class="single-widget">
							<h2>Suscribete</h2>
							<form action="#" class="searchform">
								<input type="text" placeholder="Tu email..." />
								<button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
								<p>Se te notificará cuando actualicemos <br />nuestro repositorio de libros...</p>
							</form>
						</div>
					</div>
					
				</div>
			</div>
		</div>-->
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Todos los Derechos Reservados.</p>
					<p class="pull-right">Modificado por <span><a target="_blank" href="http://www.themeum.com">Thanatos</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
	

	';
?>