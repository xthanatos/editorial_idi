<?php 
echo '

<footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="index.php?view=public">
                                Inicio
                            </a>
                        </li>
                        <li>
                            <a href="index.php?view=shop">
                                Catálogo
                            </a>
                        </li>
                        <li>
                            <a href="index.php?view=contact">
                                Contacto
                            </a>
                        </li>
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    &copy; 2017 <a href="http://www.github.com">GitHub</a>, made with love for a better web
                </p>
            </div>
        </footer>

        ';

?>