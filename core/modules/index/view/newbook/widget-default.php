<?php

$categories = CategoryData::getAll();
$authors = AuthorData::getAll();
$editorials = EditorialData::getAll();

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="img/red-idi-ico.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Red IDi</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <!--<link href="http://themes-pixeden.com/font-demos/7-stroke/Pe-icon-7-stroke.css" rel="stylesheet" type="text/css" />-->

</head>
<body>

<div class="wrapper">
        

    <?php include 'core/modules/index/layout/sidebar.php';?>
    
    <div class="main-panel">
        <!--nav header-->
        
        <?php include 'core/modules/index/layout/nav_header.php';?>
        <!--nav header-->

        <!--widget-->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                             <a href="index.php?view=books" class="btn btn-danger pull-right"><i class="fa fa-book"></i> Cancelar</a>
                                <h3 class="title">Libro</h3>
                                <br>
                                <p class="category">Registro de Nuevo Libro</p>
                            </div>
                            <hr>
                            <div class="content">
                                <div class="body" style="margin-left: 10%;margin-right: 10%">
                                      <form class="form-horizontal" role="form" method="post" action="./?action=addbook" id="addbook">
                                          <div class="form-group">
                                            <label for="inputEmail1" class="col-lg-2 control-label">ISBN</label>
                                            <div class="col-lg-10">
                                              <input type="text" name="isbn" class="form-control" id="inputEmail1" placeholder="ISBN">
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label for="inputEmail1" class="col-lg-2 control-label">Titulo</label>
                                            <div class="col-lg-10">
                                              <input type="text" name="title" required class="form-control" id="inputEmail1" placeholder="Titulo">
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label for="inputEmail1" class="col-lg-2 control-label">Subtitulo</label>
                                            <div class="col-lg-10">
                                              <input type="text" name="subtitle" class="form-control" id="inputEmail1" placeholder="Subtitulo">
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label for="inputEmail1" class="col-lg-2 control-label">Descripcion</label>
                                            <div class="col-lg-10">
                                            <textarea class="form-control" name="description" placeholder="Descripcion"></textarea>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label for="inputEmail1" class="col-lg-2 control-label">Num. Paginas</label>
                                            <div class="col-lg-4">
                                              <input type="text" name="n_pag" class="form-control" id="inputEmail1" placeholder="Num. Paginas">
                                            </div>
                                            <label for="inputEmail1" class="col-lg-2 control-label">A&ntilde;o</label>
                                            <div class="col-lg-4">
                                              <input type="text" name="year" class="form-control" id="inputEmail1" placeholder="A&ntilde;o">
                                            </div>

                                          </div>



                                          <div class="form-group">
                                            <label for="inputEmail1" class="col-lg-2 control-label">Categoria</label>
                                            <div class="col-lg-10">
                                        <select name="category_id" class="form-control">
                                        <option value="">-- SELECCIONE --</option>
                                          <?php foreach($categories as $p):?>
                                            <option value="<?php echo $p->id; ?>"><?php echo $p->name; ?></option>
                                          <?php endforeach; ?>
                                        </select>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label for="inputEmail1" class="col-lg-2 control-label">Editorial</label>
                                            <div class="col-lg-10">
                                        <select name="editorial_id" class="form-control">
                                        <option value="">-- SELECCIONE --</option>
                                          <?php foreach($editorials as $p):?>
                                            <option value="<?php echo $p->id; ?>"><?php echo $p->name; ?></option>
                                          <?php endforeach; ?>
                                        </select>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label for="inputEmail1" class="col-lg-2 control-label">Autor</label>
                                            <div class="col-lg-10">
                                        <select name="author_id" class="form-control">
                                        <option value="">-- SELECCIONE --</option>
                                          <?php foreach($authors as $p):?>
                                            <option value="<?php echo $p->id; ?>"><?php echo $p->name." ".$p->lastname; ?></option>
                                          <?php endforeach; ?>
                                        </select>
                                            </div>
                                          </div>






                                          <div class="form-group">
                                            <div class="col-lg-offset-2 col-lg-10">
                                              <button type="submit" class="btn btn-success">Agregar Libro</button>
                                              
                                            </div>
                                          </div>
                                        </form>

                                </div>

                                <div class="footer">
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-clock-o"></i> ...
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--widget-->

<?php include 'core/modules/index/layout/footer2.php';?>
        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="js/bootstrap-checkbox-radio-switch.js"></script>

	<!--  Charts Plugin -->
	<script src="js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="js/demo.js"></script>

	<script type="text/javascript">
    	$(document).ready(function(){

        	demo.initChartist();

        	$.notify({
            	icon: 'pe-7s-gift',
            	message: "Usted a elegido añadir un <b>Libro</b> ."

            },{
                type: 'success',
                timer: 4000
            });

    	});
	</script>

</html>
