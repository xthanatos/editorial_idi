<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="img/red-idi-ico.png" >
    <title>.:Contacto | Red-IDi:.</title>
    <link href="css/bootstrap_front.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="img/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<!--<li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>-->
								<li><a href="#"><i class="fa fa-envelope"></i> contactenos@redidi.org.pe</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="https://www.facebook.com/Red-IDi-107496599326428/"><i class="fa fa-facebook"></i></a></li>
								<li><a href="https://twitter.com/red_idi"><i class="fa fa-twitter"></i></a></li>
								<li><a href="https://pe.linkedin.com/company/redidi"><i class="fa fa-linkedin"></i></a></li>
								<!--<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>-->
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><img src="img/home/logo-RED-IDI.png" alt="" /></a>
						</div>
						<!--
						<div class="btn-group pull-right">
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									USA
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="">Canada</a></li>
									<li><a href="">UK</a></li>
								</ul>
							</div>
							
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									DOLLAR
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="">Canadian Dollar</a></li>
									<li><a href="">Pound</a></li>
								</ul>
							</div>
						</div>-->
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<!--<li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li>
								<li><a href="checkout.html"><i class="fa fa-crosshairs"></i> Checkout</a></li>
								<li><a href="cart.html"><i class="fa fa-shopping-cart"></i> Cart</a></li>-->
								<?php 
								if(Session::getUID()!=""){
									$u = UserData::getById(Session::getUID());
									$tmp=$u->name." ".$u->lastname;
								}else{
									$tmp="Cuenta";
								}

								?>
								<?php $usr=$tmp;?>
								<li><a href=<?php if(Session::getUID()!=""){echo "index.php?view=dashboard";}else{;echo "#";} ?>><i class="fa fa-user"></i><?php echo "$usr"; ?></a></li>

								
								<?php 
									if(Session::getUID()!=""){
									echo '<li class="dropdown"><a href="index.php?view=configuration">';
									echo '<i class="fa fa-cog"></i>Config.';
									}else{
									echo '<li class="dropdown"><a href="index.php?view=login2">';	
									echo '<i class="fa fa-lock"></i>Login';
									}
								?>
								</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	<?php
/*
if(Session::getUID()!=""){
		print "<script>window.location='index.php?view=contact';</script>";
}*/

?>
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php" >Inicio</a></li>
								<li><a href="index.php?view=shop" >Libros</a></li>
								
								<!--<li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="shop.html">Products</a></li>
										<li><a href="product-details.html">Product Details</a></li> 
										<li><a href="checkout.html">Checkout</a></li> 
										<li><a href="cart.html">Cart</a></li> 
										<li><a href="login.html">Login</a></li> 
                                    </ul>
                                </li>-->
								<!--<li class="dropdown"><a href="blog.html">Novedades<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="blog-single.html">Lista de Novedades</a></li>
										<li><a href="blog-single.html">Blog Single</a></li>
                                    </ul>
                                </li>--> 
								<!--<li><a href="404.html">404</a></li>-->
								<li><a href="index.php?view=contact" class="active">Contacto</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<!--
								<input type="text" placeholder="Búsqueda" style="width: 100%" />
							
							-->
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	 
	 <div id="contact-page" class="container">
    	<div class="bg">
	    	<div class="row">    		
	    		<div class="col-sm-12">    			   			
					<h2 class="title text-center">Ubícanos</h2>    			    				    				
					<div id="gmap" class="contact-map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d243.79242132503146!2d-77.02650908145807!3d-12.134119499721397!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105b7e3c0fe002d%3A0xf79af88ff0cf319e!2sArmendariz+445%2C+Distrito+de+Lima+18%2C+Per%C3%BA!5e0!3m2!1ses!2ses!4v1500669922978" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>			 		
			</div>    	
    		<div class="row">  	
	    		<div class="col-sm-8">
	    			<div class="contact-form">
	    				<h2 class="title text-center">Envianos un mail</h2>
	    				<div class="status alert alert-success" style="display: none"></div>
				    	<form id="main-contact-form" class="contact-form row" name="contact-form" method="post">
				            <div class="form-group col-md-6">
				                <input type="text" name="name" class="form-control" required="required" placeholder="Nombre">
				            </div>
				            <div class="form-group col-md-6">
				                <input type="email" name="email" class="form-control" required="required" placeholder="Email">
				            </div>
				            <div class="form-group col-md-12">
				                <input type="text" name="subject" class="form-control" required="required" placeholder="Asunto">
				            </div>
				            <div class="form-group col-md-12">
				                <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Pon aquí lo que quieras transmitirnos..."></textarea>
				            </div>                        
				            <div class="form-group col-md-12">
				                <input type="submit" name="submit" class="btn btn-primary pull-right" value="Enviar" >
				            </div>
				        </form>
	    			</div>
	    		</div>
	    		<div class="col-sm-4">
	    			<div class="contact-info">
	    				<h2 class="title text-center">Información de contacto</h2>
	    				<address>
	    					<p>Asociación Red IDi.</p>
							<p>Av. Armendáriz 445, 2° Piso Miraflores</p>
							<p>Lima 15074 Perú</p>
							<p>Email: contactenos@redidi.org.pe</p>
	    				</address>
	    				<div class="social-networks">
	    					<h2 class="title text-center">Redes Sociales</h2>
							<ul>
								<li>
									<a href="https://www.facebook.com/Red-IDi-107496599326428/"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="https://twitter.com/red_idi"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="https://pe.linkedin.com/company/redidi"><i class="fa fa-linkedin"></i></a>
								</li>
								
							</ul>
	    				</div>
	    			</div>
    			</div>    			
	    	</div>  
    	</div>	
    </div><!--/#contact-page-->

    <?php include 'core/modules/index/layout/footer1.php';?>

    <script src="js/front/jquery.js"></script>
	<script src="js/front/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="js/gmaps.js"></script>
	<script src="js/front/contact.js"></script>
	<script src="js/front/jquery.scrollUp.min.js"></script>
	<script src="js/front/price-range.js"></script>
    <script src="js/front/jquery.prettyPhoto.js"></script>
    <script src="js/front/main.js"></script>
</body>
</html>