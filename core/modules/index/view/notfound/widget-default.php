<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>.:Not Found | Red-IDi:.</title>
    <link href="css/bootstrap_front.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="img/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<div class="container text-center">
		<div class="logo-404">
			<a href="index.php"><img src="img/home/logo-RED-IDI.png" alt="" /></a>
		</div>
		<div class="content-404">
			<img src="img/404/404.png" class="img-responsive" alt="" style="width: 25%" />
			<h1><b>OOPSY DOOPSY!</b> No pudimos encontrar la página</h1>
			<p>Parece que te has perdido en la búsqueda...</p>
			<h2><a href="index.php">Volver al inicio</a></h2>
		</div>
	</div>

  
    <script src="js/front/jquery.js"></script>
    <script src="js/front/bootstrap.min.js"></script>
    <script src="js/front/jquery.scrollUp.min.js"></script>
    <script src="js/front/price-range.js"></script>
    <script src="js/front/jquery.prettyPhoto.js"></script>
    <script src="js/front/main.js"></script>
</body>
</html>