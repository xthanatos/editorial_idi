<?php
$book = BookData::getById($_GET["id"]);
//$categories = CategoryData::getAll();
$author = AuthorData::getById($book->author_id);
$editorial = EditorialData::getById($book->editorial_id);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="img/red-idi-ico.png" >
    <title>.:Detail | Red-IDi:.</title>
    <link href="css/bootstrap_front.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="img/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<!--<li><a href="#"><i class="fa fa-phone"></i> +987654321</a></li>-->
								<li><a href="#"><i class="fa fa-envelope"></i> contactenos@redidi.org.pe</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="https://www.facebook.com/Red-IDi-107496599326428/"><i class="fa fa-facebook"></i></a></li>
								<li><a href="https://twitter.com/red_idi"><i class="fa fa-twitter"></i></a></li>
								<li><a href="https://pe.linkedin.com/company/redidi"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><img src="img/home/logo-RED-IDI.png" alt="" /></a>
						</div>
						
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<!--<li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li>
								<li><a href="checkout.html"><i class="fa fa-crosshairs"></i> Checkout</a></li>
								<li><a href="cart.html"><i class="fa fa-shopping-cart"></i> Cart</a></li>-->
								<?php 
								if(Session::getUID()!=""){
									$u = UserData::getById(Session::getUID());
									$tmp=$u->name." ".$u->lastname;
								}else{
									$tmp="Cuenta";
								}

								?>
								<?php $usr=$tmp;?>
								<li><a href=<?php if(Session::getUID()!=""){echo "index.php?view=dashboard";}else{;echo "#";} ?> ><i class="fa fa-user"></i><?php echo "$usr"; ?></a></li>

								
								<?php 
									if(Session::getUID()!=""){
									echo '<li class="dropdown"><a href="index.php?view=configuration">';
									echo '<i class="fa fa-cog"></i>Config.';
									}else{
									echo '<li class="dropdown"><a href="index.php?view=login2">';	
									echo '<i class="fa fa-lock"></i>Login';
									}
								?>
								</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php" class="active">Inicio</a></li>
								<li><a href="index.php?view=shop" >Libros</a></li>
								
								<!--<li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="shop.html">Products</a></li>
										<li><a href="product-details.html">Product Details</a></li> 
										<li><a href="checkout.html">Checkout</a></li> 
										<li><a href="cart.html">Cart</a></li> 
										<li><a href="login.html">Login</a></li> 
                                    </ul>
                                </li>-->
								<!--<li class="dropdown"><a href="blog.html">Novedades<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="blog-single.html">Lista de Novedades</a></li>
										<li><a href="blog-single.html">Blog Single</a></li>
                                    </ul>
                                </li>--> 
								<!--<li><a href="404.html">404</a></li>-->
								<li><a href="index.php?view=contact">Contacto</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right col-sm-6 ">
							<input type="text" placeholder="Búsqueda"/>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Categoría</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<!--<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Autores
										</a>
									</h4>
								</div>
								<div id="sportswear" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="#">Autor1 </a></li>
											<li><a href="#">Autor2 </a></li>
											<li><a href="#">Autor3 </a></li>
											<li><a href="#">Autor4</a></li>
										</ul>
									</div>
								</div>
							</div>-->
							<?php $values = CategoryData::getAll(); ?>
							<?php foreach ($values as $value):?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a href="#">
											<?php echo $value->name; ?>
										</a>
									</h4>
								</div>
							</div>
							<?php endforeach; ?>
							
						</div><!--/category-products-->
					
						
						
						
						
						<div class="shipping text-center"><!--shipping-->
							<!--<img src="img/home/read_important.png" alt="" />-->
						</div><!--/shipping-->
					
					</div>

				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product">
								<img src="<?php echo $book->image; ?>" alt="" />
								
							</div>
							

						</div>
						<div class="col-sm-6">
							<div class="product-information"><!--/product-information-->
								<h2 style="margin-right: 5%"><?php echo $book->title; ?></h2>
								<p>ISBN: <?php echo $book->isbn; ?></p>
								<span>
									<span>S/. <?php echo $book->price; ?></span>
								</span>
								<!--<p><b>Disponibilidad:</b> Fisico</p>
								<p><b>Coleccion:</b> Tomo X</p>-->
								<p><b>Editorial:</b> <?php echo $editorial->name; ?></p>
								<!--<a href=""><img src="img/product-details/share.png" class="share img-responsive"  alt="" /></a>-->
							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->
					
					<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li  class="active"><a href="#details" data-toggle="tab">Descripción</a></li>
								<li><a href="#companyprofile" data-toggle="tab">Autor</a></li>
								<li><a href="#bookindex" data-toggle="tab">Indice</a></li>
								<li><a href="#tecnicalspecs" data-toggle="tab">Especificaciones</a></li>
								<!--<li><a href="#reviews" data-toggle="tab">Opiniones</a></li>-->
							</ul>
						</div>
						<div class="tab-content">
							<div class="tab-pane fade active in" id="details" >
								<div class="col-sm-10">
									<p><?php echo $book->description; ?></p>
								</div>
							</div>
							
							<div class="tab-pane fade" id="companyprofile" >
								<div class="col-sm-10">
									<p><?php echo $author->name." ".$author->lastname ?>.</p>
								</div>
							</div>
							
							<div class="tab-pane fade" id="bookindex" >
								<div class="col-sm-10">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
								</div>
							</div>
							
							<div class="tab-pane fade" id="tecnicalspecs" >
								<div class="col-sm-10">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
								</div>
							</div>
							
							<!--
							<div class="tab-pane fade active" id="reviews" >
								<div class="col-sm-12">
									<ul>
										<li><a href=""><i class="fa fa-user"></i>Thanatos</a></li>
										<li><a href=""><i class="fa fa-clock-o"></i>12:41 PM</a></li>
										<li><a href=""><i class="fa fa-calendar-o"></i>25 JUL 2017</a></li>
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
									<p><b>Escribe tu opinión</b></p>
									
									<form action="#">
										<span>
											<input type="text" placeholder="Tu Nombre"/>
											<input type="email" placeholder="Email"/>
										</span>
										<textarea name="" ></textarea>
										<b>Calificación: </b> <img src="img/product-details/rating.png" alt="" />
										<button type="button" class="btn btn-default pull-right">
											Enviar
										</button>
									</form>
								</div>
							</div>-->
							
						</div>
					</div><!--/category-tab-->
					
					<div class="recommended_items"><!--recommended_items-->
						<h2 class="title text-center">Nuevos Libros </h2>
						
						<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								<div class="item active">	
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="img/home/libro3.jpg" alt="" style="width: 65%" />
													<h2>S/ 120,00</h2>
													<p>The Legend of Zelda</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-bookmark"></i>Ver más</a>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="img/home/libro5.jpg" alt="" style="width: 65%"/>
													<h2>S/ 159,00</h2>
													<p>Las auroras de sangre</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-bookmark"></i>Ver más</a>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="img/home/libro4.jpg" alt="" style="width: 65%"/>
													<h2>S/ 10,00</h2>
													<p>El Silencio del Almendro</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-bookmark"></i>Ver más</a>
												</div>
												
											</div>
										</div>
									</div>
								</div>
								<div class="item">	
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="img/home/libro4.jpg" alt="" style="width: 65%"/>
													<h2>S/ 10,00</h2>
													<p>El Silencio del Almendro</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-bookmark"></i>Ver más</a>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="img/home/libro5.jpg" alt="" style="width: 65%"/>
													<h2>S/ 159,00</h2>
													<p>Las auroras de sangre</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-bookmark"></i>Ver más</a>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="img/home/libro3.jpg" alt="" style="width: 65%"/>
													<h2>S/ 120,00</h2>
													<p>The Legend of Zelda</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-bookmark"></i>Ver más</a>
												</div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							  </a>
							  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							  </a>			
						</div>
					</div><!--/recommended_items-->
					
				</div>
			</div>
		</div>
	</section>

	<?php include 'core/modules/index/layout/footer1.php';?>
	

  
    <script src="js/front/jquery.js"></script>
	<script src="js/front/bootstrap.min.js"></script>
	<script src="js/front/jquery.scrollUp.min.js"></script>
	<script src="js/front/price-range.js"></script>
    <script src="js/front/jquery.prettyPhoto.js"></script>
    <script src="js/front/main.js"></script>
</body>
</html>