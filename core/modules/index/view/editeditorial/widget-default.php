<?php $user = EditorialData::getById($_GET["id"]);?>


<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="img/red-idi-ico.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Red IDi</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <!--<link href="http://themes-pixeden.com/font-demos/7-stroke/Pe-icon-7-stroke.css" rel="stylesheet" type="text/css" />-->

</head>
<body>

<div class="wrapper">
        

    <?php include 'core/modules/index/layout/sidebar.php';?>
    
    <div class="main-panel">
        <!--nav header-->
        
        <?php include 'core/modules/index/layout/nav_header.php';?>
        <!--nav header-->

        <!--widget-->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                             <a href="index.php?view=editorials" class="btn btn-danger pull-right"><i class="fa fa-book"></i> Cancelar</a>
                                <h3 class="title">Edición de Categoría</h3>
                                <br>
                                <p class="category">La <b>editorial</b> solo se modificara si escribes algo, en caso contrario no se modifica.</p>
                            </div>
                            <hr>
                            <div class="content">
                                <div class="body" style="margin-left: 10%;margin-right: 10%">
                                      <form class="form-horizontal" method="post" id="addproduct" action="index.php?action=updateeditorial" role="form">
                                      <div class="form-group">
                                        <label for="inputEmail1" class="col-lg-2 control-label">Nombre*</label>
                                        <div class="col-md-6">
                                          <input type="text" name="name" value="<?php echo $user->name;?>" class="form-control" id="name" placeholder="Nombre">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                        <input type="hidden" name="user_id" value="<?php echo $user->id;?>">
                                          <button type="submit" class="btn btn-success">Actualizar Editorial</button>
                                        </div>
                                      </div>
                                    </form>
                                </div>

                                <div class="footer">
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-clock-o"></i> ...
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--widget-->

<?php include 'core/modules/index/layout/footer2.php';?>
        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="js/bootstrap-checkbox-radio-switch.js"></script>

	<!--  Charts Plugin -->
	<script src="js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="js/demo.js"></script>

	<script type="text/javascript">
    	$(document).ready(function(){

        	demo.initChartist();

        	$.notify({
            	icon: 'pe-7s-gift',
            	message: "Modo edicion de <b>Editorial</b> proceda con precaución ."

            },{
                type: 'warning',
                timer: 4000
            });

    	});
	</script>

</html>
