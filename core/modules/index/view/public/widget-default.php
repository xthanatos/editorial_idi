<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="img/red-idi-ico.png" >
    <title>.:Inicio | Red-IDi:.</title>
    <link href="css/bootstrap_front.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="img/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/ico/apple-touch-icon-57-precomposed.png">
    <style type="text/css">
    	/*body {
    		background-color: lightblue;
		};
		li {
    		background-color: lightblue;
		}
		i {
    		background-color: lightblue;
		}*/

    </style>
</head><!--/head-->

<body onload="loadEvents()">
	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<!--<li><a href="#"><i class="fa fa-phone"></i> +987654321</a></li>-->
								<li><a href="#"><i class="fa fa-envelope"></i> contactenos@redidi.org.pe</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="https://www.facebook.com/Red-IDi-107496599326428/"><i class="fa fa-facebook"></i></a></li>
								<li><a href="https://twitter.com/red_idi"><i class="fa fa-twitter"></i></a></li>
								<li><a href="https://pe.linkedin.com/company/redidi"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><img src="img/home/logo-RED-IDI.png" alt="" /></a>
						</div>
						
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<!--<li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li>
								<li><a href="checkout.html"><i class="fa fa-crosshairs"></i> Checkout</a></li>
								<li><a href="cart.html"><i class="fa fa-shopping-cart"></i> Cart</a></li>-->
								<?php 
								if(Session::getUID()!=""){
									$u = UserData::getById(Session::getUID());
									$tmp=$u->name." ".$u->lastname;
								}else{
									$tmp="Cuenta";
								}

								?>
								<?php $usr=$tmp;?>
								<li><a href=<?php if(Session::getUID()!=""){echo "index.php?view=dashboard";}else{;echo "#";} ?> ><i class="fa fa-user"></i><?php echo "$usr"; ?></a></li>

								
								<?php 
									if(Session::getUID()!=""){
									echo '<li class="dropdown"><a href="index.php?view=configuration">';
									echo '<i class="fa fa-cog"></i>Config.';
									}else{
									echo '<li class="dropdown"><a href="index.php?view=login2">';	
									echo '<i class="fa fa-lock"></i>Login';
									}
								?>
								</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php" class="active">Inicio</a></li>
								<li><a href="index.php?view=shop" >Libros</a></li>
								
								<!--<li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="shop.html">Products</a></li>
										<li><a href="product-details.html">Product Details</a></li> 
										<li><a href="checkout.html">Checkout</a></li> 
										<li><a href="cart.html">Cart</a></li> 
										<li><a href="login.html">Login</a></li> 
                                    </ul>
                                </li>-->
								<!--<li class="dropdown"><a href="blog.html">Novedades<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="blog-single.html">Lista de Novedades</a></li>
										<li><a href="blog-single.html">Blog Single</a></li>
                                    </ul>
                                </li>--> 
								<!--<li><a href="404.html">404</a></li>-->
								<li><a href="index.php?view=contact">Contacto</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right ">
							<!--
								<input type="text" placeholder="Búsqueda" style="width: 100%" />
							
							-->
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	
	<!-- modal -->
	<!--js modal-->
	
	<script type="text/javascript">
    	function loadEvents(){
        	$('#infoxd').modal('show');
    	}
    	
	</script>
<!--
<style type="text/css">
	.modal-header-info {
    color:#fff;
    padding:9px 15px;
    border-bottom:1px solid #eee;
    background-color: #5bc0de;
    -webkit-border-top-left-radius: 5px;
    -webkit-border-top-right-radius: 5px;
    -moz-border-radius-topleft: 5px;
    -moz-border-radius-topright: 5px;
     border-top-left-radius: 5px;
     border-top-right-radius: 5px;
}
</style>
-->

<div class="modal fade" id="infoxd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                
                <div class="modal-body">
                	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                	<h2 class="title text-center">Nuevos Eventos </h2>
						
						<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel" data-interval"1000">
							<div class="carousel-inner">
								<div class="item active" align="center">	
									<img src="img/home/libro3.jpg" alt="" style="width: 80%" />
								</div>
								<div class="item" align="center">	
									<img src="img/home/libro4.jpg" alt="" style="width: 80%"/>
								</div>
							</div>
							 		
						</div>

<!--
                	<img src="https://scontent.flim5-4.fna.fbcdn.net/v/t1.0-9/19510289_10156240943371521_2122088440784369055_n.jpg?oh=b44c06f7d6f207babadb5cd302f5500e&oe=5A396DBA" class="girl img-responsive" alt="" />-->
                </div>
                
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

	<!-- modal -->

	<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						
						<div class="carousel-inner">
							<div class="item active">
								<div class="col-sm-6">
									<h1><span>RED</span>-IDi</h1>
									<h2>Investigación, Desarrollo e Innovación</h2>
									<p>Conecta ciencia y tecnología, investigadores y científicos con el sector empresarial y gubernamental. </p>
									<button type="button" class="btn btn-default get">Enterate de mas</button>
								</div>
								<div class="col-sm-6">
									<img src="img/home/logo_libros2.png" class="girl img-responsive" alt="" />
									
								</div>
							</div>
							<div class="item">
								<div class="col-sm-6">
									<h1><span>RED</span>-IDi</h1>
									<h2>Investigación, Desarrollo e Innovación</h2>
									<p>Conecta ciencia y tecnología, investigadores y científicos con el sector empresarial y gubernamental. </p>
									<button type="button" class="btn btn-default get">Enterate de mas</button>
								</div>
								<div class="col-sm-6">
									<img src="img/home/logo_libros1.png" class="girl img-responsive" alt="" width="60%" />
									
								</div>
							</div>
							
							<div class="item">
								<div class="col-sm-6">
									<h1><span>RED</span>-IDi</h1>
									<h2>Investigación, Desarrollo e Innovación</h2>
									<p>Conecta ciencia y tecnología, investigadores y científicos con el sector empresarial y gubernamental. </p>
									<button type="button" class="btn btn-default get">Enterate de mas</button>
								</div>
								<div class="col-sm-6">
									<img src="img/home/logo_libros3.png" class="girl img-responsive" alt=""  width="60%"/>
								</div>
							</div>
							
						</div>
						
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Categoría</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<!--<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Autores
										</a>
									</h4>
								</div>
								<div id="sportswear" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="#">Autor1 </a></li>
											<li><a href="#">Autor2 </a></li>
											<li><a href="#">Autor3 </a></li>
											<li><a href="#">Autor4</a></li>
										</ul>
									</div>
								</div>
							</div>-->
							<?php $values = CategoryData::getAll(); ?>
							<?php foreach ($values as $value):?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a href="#">
											<?php echo $value->name; ?>
										</a>
									</h4>
								</div>
							</div>
							<?php endforeach; ?>
							
						</div><!--/category-products-->
						
						<!--brands_products-->
						<!--
						<div class="brands_products">
							<h2>Editoriales</h2>
							<div class="brands-name">
								<ul class="nav nav-pills nav-stacked">
									<li><a href="#"> <span class="pull-right">(50)</span>EDUNI</a></li>
									<li><a href="#"> <span class="pull-right">(56)</span>FONDO EDITORIAL PUCP</a></li>
									<li><a href="#"> <span class="pull-right">(27)</span>CENTRO EDITORIAL UPCH</a></li>
									<li><a href="#"> <span class="pull-right">(32)</span>FONDO EDITORIAL UNMSM</a></li>
									<li><a href="#"> <span class="pull-right">(5)</span>FONDO EDITORIAL UNALM</a></li>
								</ul>
							</div>
						</div>-->
						<!--/brands_products-->
						

						<!--price-range-->
						<!--
						<div class="price-range">
							<h2>Año de Publicación</h2>
							<div class="well text-center">
								 <input type="text" class="span2" value="" data-slider-min="1980" data-slider-max="2017" data-slider-step="5" data-slider-value="[1980,2017]" id="sl2" ><br />
								 <b class="pull-left"> 1980</b> <b class="pull-right">2017</b>
							</div>
						</div>-->
						<!--/price-range-->
						
						<div class="shipping text-center"><!--shipping-->
							<!--<img src="img/home/read_important.png" alt="" />-->
						</div><!--/shipping-->
					
					</div>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Nuestros Libros</h2>
						<?php $bookz = BookData::getTop(6); ?>
						<?php foreach ($bookz as $booz): ?>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<div class="productinfo text-center">
											<img src="<?php echo $booz->image; ?>" alt="" />
											<h3><?php echo substr($booz->title,0,20)."..."; ?></h3>
											<p><?php if (strlen($booz->subtitle)==null) {
												echo "Sin Subtítulo";
											}else {
												echo substr($booz->subtitle,0,23);
											} ; ?></p>
											<a href="index.php?view=bookdetails&id=<?php echo $booz->id;?>" class="btn btn-default add-to-cart"><i class="fa fa-bookmark"></i>Ver más</a>
											<h2>S/ <?php echo $booz->price; ?></h2>
											<!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-bookmark"></i>Ver más</a>-->
										</div>
										<div class="product-overlay">
											<div class="overlay-content">
												<h3 style="margin-left: 15px;margin-right: 15px;"><?php echo $booz->title; ?></h3>
												<p style="margin-left: 15px;margin-right: 15px;"><?php echo $booz->subtitle; ?></p>
												<h4 style="margin-left: 15px;margin-right: 15px;"><?php echo $booz->name." ".$booz->lastname ?></h4>
												<p style="text-align: justify; margin-left: 15px;margin-right: 15px; border: 1px;color: black;"><?php echo $booz->description; ?></p>
												<br>

												<a href="index.php?view=bookdetails&id=<?php echo $booz->id;?>" class="btn btn-default add-to-cart"><i class="fa fa-bookmark"></i>Ver más</a>
											</div>
										</div>
								</div>
								
							</div>
						</div>
						<?php endforeach; ?>
						
						
						
					</div><!--features_items-->

					<h2 class="title text-center">Libros por Editorial </h2>
					<?php $books = BookData::getAll(); ?>
					<?php $editoriales = EditorialData::getAll(); ?>
					<div class="category-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#1" data-toggle="tab">EDUNI</a></li>
								<li><a href="#2" data-toggle="tab">F.E. PUCP</a></li>
								<li><a href="#3" data-toggle="tab">C.E. UPCH</a></li>
								<li><a href="#4" data-toggle="tab">F.E. UNMSM</a></li>
								<li><a href="#5" data-toggle="tab">F.E. UNALM</a></li>
							</ul>
						</div>
						<div class="tab-content">
							
							<div class="tab-pane fade active in" id="1" >

								<?php foreach ($books as $book):?>
								<?php if ($book->editorial_id == 1) { ?>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
													<img src="<?php echo $book->image; ?>" alt="" />
													<h2>S/ <?php echo $book->price; ?></h2>
													<p><?php echo substr($book->title,0,23); ?></p>
													<a href="index.php?view=bookdetails&id=<?php echo $book->id;?>" class="btn btn-default add-to-cart"><i class="fa fa-bookmark"></i>Ver más</a>
											</div>
											
										</div>
									</div>
								</div>
								<?php } ?>
								<?php endforeach; ?>
								
							</div>
							
							<div class="tab-pane fade" id="2" >

								<?php foreach ($books as $book):?>
								<?php if ($book->editorial_id == 2) { ?>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
													<img src="<?php echo $book->image; ?>" alt="" />
													<h2>S/ <?php echo $book->price; ?></h2>
													<p><?php echo substr($book->title,0,23); ?></p>
													<a href="index.php?view=bookdetails&id=<?php echo $book->id;?>" class="btn btn-default add-to-cart"><i class="fa fa-bookmark"></i>Ver más</a>
											</div>
											
										</div>
									</div>
								</div>
								<?php } ?>
								<?php endforeach; ?>
								
							</div>

							<div class="tab-pane fade" id="3" >

								<?php foreach ($books as $book):?>
								<?php if ($book->editorial_id == 3) { ?>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
													<img src="<?php echo $book->image; ?>" alt="" />
													<h2>S/ <?php echo $book->price; ?></h2>
													<p><?php echo substr($book->title,0,23); ?></p>
													<a href="index.php?view=bookdetails&id=<?php echo $book->id;?>" class="btn btn-default add-to-cart"><i class="fa fa-bookmark"></i>Ver más</a>
											</div>
											
										</div>
									</div>
								</div>
								<?php } ?>
								<?php endforeach; ?>
								
							</div>

							<div class="tab-pane fade" id="4" >

								<?php foreach ($books as $book):?>
								<?php if ($book->editorial_id == 4) { ?>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
													<img src="<?php echo $book->image; ?>" alt="" />
													<h2>S/ <?php echo $book->price; ?></h2>
													<p><?php echo substr($book->title,0,23); ?></p>
													<a href="index.php?view=bookdetails&id=<?php echo $book->id;?>" class="btn btn-default add-to-cart"><i class="fa fa-bookmark"></i>Ver más</a>
											</div>
											
										</div>
									</div>
								</div>
								<?php } ?>
								<?php endforeach; ?>
								
							</div>

							<div class="tab-pane fade" id="5" >

								<?php foreach ($books as $book):?>
								<?php if ($book->editorial_id == 5) { ?>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
													<img src="<?php echo $book->image; ?>" alt="" />
													<h2>S/ <?php echo $book->price; ?></h2>
													<p><?php echo substr($book->title,0,23); ?></p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-bookmark"></i>Ver más</a>
											</div>
											
										</div>
									</div>
								</div>
								<?php } ?>
								<?php endforeach; ?>
								
							</div>
							
							
						</div>
					</div><!--/category-tab-->
					
					
					
				</div>
			</div>
		</div>
	</section>
	
	<?php include 'core/modules/index/layout/footer1.php';?>
	
    <script src="js/front/jquery.js"></script>
	<script src="js/front/bootstrap.min.js"></script>
	<script src="js/front/jquery.scrollUp.min.js"></script>
	<script src="js/front/price-range.js"></script>
    <script src="js/front/jquery.prettyPhoto.js"></script>
    <script src="js/front/main.js"></script>
</body>
</html>