<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="img/red-idi-ico.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Red IDi</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <!--<link href="http://themes-pixeden.com/font-demos/7-stroke/Pe-icon-7-stroke.css" rel="stylesheet" type="text/css" />-->

</head>
<body>

<div class="wrapper">
    <?php include 'core/modules/index/layout/sidebar.php';?>

    <div class="main-panel">
        <?php include 'core/modules/index/layout/nav_header.php';?>


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Repositorio de libros</h4>
                                <p class="category">Buscar libro por titulo o por ISBN</p>
                                    <form id="searchp">
                                            <div class="row">
                                                
                                                <div class="col-md-7">
                                                    <div class="col-xs-4">
                                                        <input type="hidden" name="view" value="sell">
                                                        <input type="text" id="product_code" name="product" class="form-control">
                                                    </div>
                                                    <div class="col-xs-3">
                                                    <?php $cats = CategoryData::getAll();?>
                                                        <input type="hidden" name="view" value="editorial">
                                                        <select class="form-control" id="sel1" input-lg>
                                                                <option>Todas Categorias</option>
                                                                <?php foreach($cats as $cat):?>
                                                                <option value="<?php echo $item->id; ?>"> <?php echo $cat->name; ?></option>
                                                                <?php endforeach; ?>
                                                        </select>

                                                    </div>
                                                    <div class="col-xs-3">
                                                    <?php $items = EditorialData::getAll();?>
                                                        <input type="hidden" name="view" value="editorial">
                                                        <select class="form-control" id="sel1" input-lg>
                                                                <option>Todas Editoriales</option>
                                                                <?php foreach($items as $item):?>
                                                                <option value="<?php echo $item->id; ?>"> <?php echo $item->name; ?></option>
                                                                <?php endforeach; ?>
                                                        </select>

                                                    </div>

                                                    <div class="col-md-2">
                                                        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> Buscar</button>
                                                    </div>

                                                    <script>
                                                    //jQuery.noConflict();

                                                    $(document).ready(function(){
                                                        $("#searchp").on("submit",function(e){
                                                            e.preventDefault();
                                                            
                                                            $.get("./?action=searchbook",$("#searchp").serialize(),function(data){
                                                                $("#show_search_results").html(data);
                                                            });
                                                            $("#product_code").val("");

                                                        });
                                                        });
                                                    </script>
                                                </div>
                                            </div>
                                    </form>
                            </div>
                            

                            <div class="content">
                                <div class="body"></div>
                                        <?php if(isset($_SESSION["cart"])):
                                            $total = 0;
                                            ?>
                                            <div class="container">
                                            <div class="row">
                                            <div class="col-md-12">
                                            <h2>Lista de libros</h2>

                                            <form class="form-horizontal" role="form" method="post" action="./?action=process">
                                              <div class="form-group">

                                                <div class="col-lg-3">
                                                <label class="control-label">Cliente</label>
                                            <select name="client_id" required class="form-control">
                                            <option value="">-- SELECCIONE --</option>
                                              <?php foreach(ClientData::getAll() as $p):?>
                                                <option value="<?php echo $p->id; ?>"><?php echo $p->name." ".$p->lastname; ?></option>
                                              <?php endforeach; ?>
                                            </select>
                                                </div>

                                                <div class="col-lg-3">
                                                <label class="control-label">Inicio</label>
                                                  <input type="date" name="start_at" required class="form-control" placeholder="Email">
                                                </div>
                                                <div class="col-lg-3">
                                                <label class="control-label">Fin</label>
                                                  <input type="date" name="finish_at" required class="form-control" placeholder="Email">
                                                </div>
                                                <div class="col-lg-2">
                                                <label class="control-label"><br></label>
                                                  <input type="submit" value="Procesar" class="btn btn-primary btn-block" placeholder="Email">
                                                </div>
                                                <div class="col-lg-1">
                                                <label class="control-label"><br></label>
                                                <a href="./?action=clearcart" class="btn btn-danger btn-block"><i class="fa fa-trash"></i></a>
                                                </div>
                                              </div>

                                            </form>
                                            <table class="table table-bordered table-hover">
                                            <thead>
                                                <th style="width:40px;">Codigo</th>
                                                <th style="width:40px;">Ejemplar</th>
                                                <th>Titulo</th>
                                                <th></th>
                                            </thead>
                                            <?php foreach($_SESSION["cart"] as $p):
                                            $book = BookData::getById($p["book_id"]);
                                            $item = ItemData::getById($p["item_id"]);

                                            ?>
                                            <tr >
                                                <td><?php echo $book->isbn; ?></td>
                                                <td ><?php echo $item->code; ?></td>
                                                <td ><?php echo $book->title; ?></td>
                                                <td style="width:30px;">
                                                <a href="index.php?view=clearcart&product_id=<?php echo $book->id; ?>" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Cancelar</a>
                                                </td>
                                            </tr>

                                            <?php endforeach; ?>
                                            </table>
                                            </div>
                                            </div>
                                            </div>
                                            <br><br><br><br><br>
                                        <?php endif; ?>


                                <div class="footer">

                                    
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-clock-o"></i> Potenciado con motor de Busqueda Relacional
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>


        <?php include 'core/modules/index/layout/footer2.php';?>

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="js/bootstrap-checkbox-radio-switch.js"></script>

	<!--  Charts Plugin -->
	<script src="js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="js/demo.js"></script>

	<script type="text/javascript">
    	$(document).ready(function(){

        	demo.initChartist();

        	$.notify({
            	icon: 'pe-7s-gift',
            	message: "Bienvenido al modo <b>Administrador</b> ."

            },{
                type: 'info',
                timer: 4000
            });

    	});
	</script>

</html>
