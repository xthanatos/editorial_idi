<?php  
	/*const HOST = 'localhost';
	const USER = 'root';
	const PASSWD = 'cmc';
	const DB = 'library2';

	$url = basename($_SERVER["PHP_SELF"]);
	$limit_end = 6;
	$init = ($ini-1)*$limit_end;

	$count = "SELECT COUNT(*) FROM book";
	$select = "SELECT * FROM book as b INNER JOIN author as a WHERE b.author_id = a.id LIMIT $init, $limit_end";

	$mysql = new mysqli(HOST, USER, PASSWD, DB);

	if($mysql->connect_error){
		die("Error al conectarse al servidor");
	}
	else{
		$num = $mysql->query($count);
		$x = $num->fetch_array();

		$total = ceil($x[0]/$limit_end);

		echo "<div class='features_items'>";
		echo "<h2 class='title text-center'>Listado de Libros</h2>";

		$c = $mysql->query($select);
		while ($rows = $c->fetch_array(MYSQLI_ASSOC)) {
			echo "<div class='col-sm-4'>";
			echo "<div class='product-image-wrapper'>";
			echo "<div class='single-products'>";
			echo "<div class='productinfo text-center'>";
			echo "<img src='".$rows['image']."' alt=''/>";
			echo "<h3>".substr($rows['title'],0,23)."</h3>";
			echo "<h2>S/ ".$rows['price']."</h2>";
			echo "</div>";
			echo "<div class='product-overlay'>";
			echo "<div class='overlay-content'>";
			echo "<h3>".$rows['title']."</h3>";
			echo "<p>".$rows['subtitle']."</p>";
			echo "<h4>".$rows['name']." ".$rows['lastname']."</h4>";
			echo "<p style='text-align: justify; margin-left: 15px;margin-right: 15px; border: 1px;color: black;'>".$rows['description']."</p>";
			echo "<br>";
			echo "<a href='#' class='btn btn-default add-to-cart'><i class='fa fa-bookmark'></i>Ver más</a>";
			echo "</div></div></div></div></div>";
		}
		echo "</div>";

		echo "<div align='center'>";
		//echo "<div class='pagination'>";
		echo "<ul class='pagination'>";

		if(($ini-1)==0){
			echo "<li><a href='#'>&laquo;</a></li>";
		}
		else {
			echo "<li><a href='$url?pos=".($ini-1)."'><b>&laquo;</b></a></li>";
		}

		for ($k=1; $k <= $total; $k++) { 
			if ($ini == $k){
				echo "<li><a href='#'><b>".$k."</b></a></li>";
			}
			else {
				echo "<li><a href='$url?pos=$k'>".$k."</a></li>";
			}
		}

		if ($ini == $total){
			echo "<li><a href='#'>&raquo;</a></li>";
		}
		else {
			echo "<li><a href='$url?pos=".($ini+1)."'><b>&raquo;</b></a></li>";
		}

		echo "</ul>";
		echo "</div>";
	}*/


	$page = 1;
	if(array_key_exists('pg', $_GET)){
		$page = $_GET['pg'];
	}

	$mysqli = new mysqli("localhost","root","","library3");
	$mysqli->set_charset("utf8");
	if ($mysqli->connect_errno) {
		printf("Connect failed: %s\n", $mysqli->connect_error);
		exit();
	}

	$conteo_query =  $mysqli->query("SELECT COUNT(*) as conteo FROM book");
		    $conteo = "";
		    if($conteo_query){
		    	while($obj = $conteo_query->fetch_object()){ 
		    	 	$conteo =$obj->conteo; 
		    	}
		    }
		    $conteo_query->close(); 
		    unset($obj);

	$max_num_paginas = intval($conteo/4);

	$segmento = $mysqli->query("SELECT * FROM book as b INNER JOIN author as a WHERE b.author_id = a.id LIMIT ".(($page-1)*6).", 6 ");

	if ($segmento){
		echo "<div class='features_items'>";
		echo "<h2 class='title text-center'>Listado de Libros</h2>";
		while ($obj2 = $segmento->fetch_object()) {
			echo "<div class='col-sm-4'>";
			echo "<div class='product-image-wrapper'>";
			echo "<div class='single-products'>";
			echo "<div class='productinfo text-center'>";
			echo "<img src='".$obj2->image."' alt=''/>";
			echo "<h3>".substr($obj2->title,0,23)."</h3>";
			echo "<h2>S/ ".$obj2->price."</h2>";
			echo "</div>";
			echo "<div class='product-overlay'>";
			echo "<div class='overlay-content'>";
			echo "<h3>".$obj2->title."</h3>";
			echo "<p>".$obj2->subtitle."</p>";
			echo "<h4>".$obj2->name." ".$obj2->lastname."</h4>";
			echo "<p style='text-align: justify; margin-left: 15px;margin-right: 15px; border: 1px;color: black;'>".$obj2->description."</p>";
			echo "<br>";
			echo "<a href='index.php?view=bookdetails&id=".($obj2->id-5)."' class='btn btn-default add-to-cart'><i class='fa fa-bookmark'></i>Ver más</a>";
			echo "</div></div></div></div></div>";
		}
		echo "</div>";
	}

	echo "<div align='center'>";
		//echo "<div class='pagination'>";
		echo "<ul class='pagination'>";

	for($i=0; $i<$max_num_paginas;$i++){
	    echo '<li><a href="index.php?view=shop?pg='.($i+1).'">'.($i+1).'</a></li>';
	}

	echo "</ul>";
	echo "</div>";


?>