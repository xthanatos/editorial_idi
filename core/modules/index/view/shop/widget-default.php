<!--<?php  
	if (isset($_GET['pos']))
		$ini = $_GET['pos'];
	else
		$ini = 1;
?>-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="img/red-idi-ico.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="img/red-idi-ico.png" >
    <title>.:Tienda | Red-IDi:.</title>
    <link href="css/bootstrap_front.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="img/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/ico/apple-touch-icon-57-precomposed.png">

   
    
    <!--<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="js/ajax.js"></script>

    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/foundation.min.css">-->

</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<!--<li><a href="#"><i class="fa fa-phone"></i> +987654321</a></li>-->
								<li><a href="#"><i class="fa fa-envelope"></i> contactenos@redidi.org.pe</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="https://www.facebook.com/Red-IDi-107496599326428/"><i class="fa fa-facebook"></i></a></li>
								<li><a href="https://twitter.com/red_idi"><i class="fa fa-twitter"></i></a></li>
								<li><a href="https://pe.linkedin.com/company/redidi"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><img src="img/home/logo-RED-IDI.png" alt="" /></a>
						</div>
						
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<!--<li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li>
								<li><a href="checkout.html"><i class="fa fa-crosshairs"></i> Checkout</a></li>
								<li><a href="cart.html"><i class="fa fa-shopping-cart"></i> Cart</a></li>-->
								<?php 
								if(Session::getUID()!=""){
									$u = UserData::getById(Session::getUID());
									$tmp=$u->name." ".$u->lastname;
								}else{
									$tmp="Cuenta";
								}

								?>
								<?php $usr=$tmp;?>
								<li><a href=<?php if(Session::getUID()!=""){echo "index.php?view=dashboard";}else{;echo "#";} ?> ><i class="fa fa-user"></i><?php echo "$usr"; ?></a></li>

								
								<?php 
									if(Session::getUID()!=""){
									echo '<li class="dropdown"><a href="index.php?view=configuration">';
									echo '<i class="fa fa-cog"></i>Config.';
									}else{
									echo '<li class="dropdown"><a href="index.php?view=login2">';	
									echo '<i class="fa fa-lock"></i>Login';
									}
								?>
								</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php" >Inicio</a></li>
								<li><a href="index.php?view=shop" class="active">Libros</a></li>
								
								<!--<li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="shop.html">Products</a></li>
										<li><a href="product-details.html">Product Details</a></li> 
										<li><a href="checkout.html">Checkout</a></li> 
										<li><a href="cart.html">Cart</a></li> 
										<li><a href="login.html">Login</a></li> 
                                    </ul>
                                </li>-->
                               	<!--
								<li class="dropdown"><a href="blog.html">Novedades<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="blog-single.html">Lista de Novedades</a></li>
										<li><a href="blog-single.html">Blog Single</a></li>
                                    </ul>
                                </li>-->
								<!--<li><a href="404.html">404</a></li>-->
								<li><a href="index.php?view=contact">Contacto</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right col-sm-6 ">
							<input type="text" placeholder="Búsqueda"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	<section id="advertisement">
		<div class="container">
			<img src="images/shop/advertisement.jpg" alt="" />
		</div>
	</section>
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Categoría</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<!--<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Autores
										</a>
									</h4>
								</div>
								<div id="sportswear" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="#">Autor1 </a></li>
											<li><a href="#">Autor2 </a></li>
											<li><a href="#">Autor3 </a></li>
											<li><a href="#">Autor4</a></li>
										</ul>
									</div>
								</div>
							</div>-->
							<?php $values = CategoryData::getAll(); ?>
							<?php foreach ($values as $value):?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a href="#">
											<?php echo $value->name; ?>
										</a>
									</h4>
								</div>
							</div>
							<?php endforeach; ?>
							
						</div><!--/category-products-->
					
						<div class="brands_products"><!--brands_products-->
							<h2>Editoriales</h2>
							<div class="brands-name">
								<ul class="nav nav-pills nav-stacked">
									<li><a href="#"> <span class="pull-right">(50)</span>EDUNI</a></li>
									<li><a href="#"> <span class="pull-right">(56)</span>FONDO EDITORIAL PUCP</a></li>
									<li><a href="#"> <span class="pull-right">(27)</span>CENTRO EDITORIAL UPCH</a></li>
									<li><a href="#"> <span class="pull-right">(32)</span>FONDO EDITORIAL UNMSM</a></li>
									<li><a href="#"> <span class="pull-right">(5)</span>FONDO EDITORIAL UNALM</a></li>
								</ul>
							</div>
						</div><!--/brands_products-->
						
						<div class="price-range"><!--price-range-->
							<h2>Año de Publicación</h2>
							<div class="well text-center">
								 <input type="text" class="span2" value="" data-slider-min="1980" data-slider-max="2017" data-slider-step="5" data-slider-value="[1980,2017]" id="sl2" ><br />
								 <b class="pull-left"> 1980</b> <b class="pull-right">2017</b>
							</div>
						</div><!--/price-range-->
						
						<div class="shipping text-center"><!--shipping-->
							<!--<img src="img/home/read_important.png" alt="" />-->
						</div><!--/shipping-->
					
					</div>
				</div>
				

				<div class="col-sm-9 padding-right">

					<!--<div class="paginacion">

					</div>
					
					<div class="links">

					</div>	-->

					


					<div>
						<?php include('paginar.php'); ?>
					</div>

				</div>


			</div>
		</div>
	</section>
	
	<?php include 'core/modules/index/layout/footer1.php';?>
  
    <script src="js/front/jquery.js"></script>
	<script src="js/front/bootstrap.min.js"></script>
	<script src="js/front/jquery.scrollUp.min.js"></script>
	<script src="js/front/price-range.js"></script>
    <script src="js/front/jquery.prettyPhoto.js"></script>
    <script src="js/front/main.js"></script>
</body>
</html>