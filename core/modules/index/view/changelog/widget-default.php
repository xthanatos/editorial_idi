<div class="row">
<div class="col-md-12">
<h1>Sistema de Editoriales</h1>
<p>Bienvenido a <b>EdSys</b> un Sistema de editoriales util para realizar consultas de libros con los que cuente su repositorio.</p>
<h4> v1.5</h4>
<ul>
	<li>Correccion de bug menores.</li>
	<!--<li>Descargar archivo Word con los datos de una cita.</li>-->
</ul>


<h4> v1.2</h4>
<ul>
	<li>Nuevos campos en libros</li>
	<li>Nuevos campos en usuarios </li>
	<li>Nuevos campos al clientes.</li>
	<!--<li>Reportes por medico, paciente, rango de fecha, estado y tipo de pago +descarga en formato word</li>-->
</ul>
<h4> v1.1</h4>
<ul>
	<li>+ Vista del Calendario</li>
	<li>Correcion del Bug al agregar libros</li>
</ul>
<h4> v1.0</h4>
<ul>
	<li>Gestion de Libros</li>
	<li>Gestion de Autores</li>
	<li>Gestion de Clientes</li>
	<li>Gestion de Usuarios con Acceso al Sistema</li>
	<li>Buscador avanzado por : Palabra clave.</li>
</ul>
<p>Thanatos &copy; 2017</p>
</div>
</div>
