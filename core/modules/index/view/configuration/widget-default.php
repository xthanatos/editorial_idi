<?php

if(Session::getUID()!=""){
		/**print "<script>window.location='index.php?view=dashboard';</script>";*/
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="img/red-idi-ico.png" >
    <title>.:Login | Red-IDi:.</title>
    <link href="css/bootstrap_front.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="img/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<!--<li><a href="#"><i class="fa fa-phone"></i> +987654321</a></li>-->
								<li><a href="#"><i class="fa fa-envelope"></i> contactenos@redidi.org.pe</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="https://www.facebook.com/Red-IDi-107496599326428/"><i class="fa fa-facebook"></i></a></li>
								<li><a href="https://twitter.com/red_idi"><i class="fa fa-twitter"></i></a></li>
								<li><a href="https://pe.linkedin.com/company/redidi"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><img src="img/home/logo-RED-IDI.png" alt="" /></a>
						</div>
						
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<!--<li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li>
								<li><a href="checkout.html"><i class="fa fa-crosshairs"></i> Checkout</a></li>
								<li><a href="cart.html"><i class="fa fa-shopping-cart"></i> Cart</a></li>-->
								<?php 
								if(Session::getUID()!=""){
									$u = UserData::getById(Session::getUID());
									$tmp=$u->name." ".$u->lastname;
								}else{
									$tmp="Cuenta";
								}

								?>
								<?php $usr=$tmp;?>
								<li><a href=<?php if(Session::getUID()!=""){echo "index.php?view=dashboard";}else{;echo "#";} ?>><i class="fa fa-user"></i><?php echo "$usr"; ?></a></li>

								
								<?php 
									if(Session::getUID()!=""){
									echo '<li class="dropdown"><a href="index.php?view=configuration">';
									echo '<i class="fa fa-cog"></i>Config.';
									}else{
									echo '<li class="dropdown"><a href="index.php?view=login2">';	
									echo '<i class="fa fa-lock"></i>Login';
									}
								?>
								</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php" class="active">Inicio</a></li>
								<li><a href="index.php?view=shop" >Libros</a></li>
								
								<!--<li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="shop.html">Products</a></li>
										<li><a href="product-details.html">Product Details</a></li> 
										<li><a href="checkout.html">Checkout</a></li> 
										<li><a href="cart.html">Cart</a></li> 
										<li><a href="login.html">Login</a></li> 
                                    </ul>
                                </li>-->
								<!--<li class="dropdown"><a href="blog.html">Novedades<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="blog-single.html">Lista de Novedades</a></li>
										<li><a href="blog-single.html">Blog Single</a></li>
                                    </ul>
                                </li>--> 
								<!--<li><a href="404.html">404</a></li>-->
								<li><a href="index.php?view=contact">Contacto</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right col-sm-6 ">
							<input type="text" placeholder="Búsqueda"/>
						</div>
					</div>
				</div>
			</div><!--/header-bottom-->
	</header><!--/header-->
	
	<section id="form"><!--form-->
		<div class="container" >
			<div class="row"  style="height: 100%" align="text-center">
				<div class="col-sm-6 " style="margin-left: 20%;margin-right: 20%;">
					<div class="signup-form"><!--sign up form-->
						<h2 style="text-align: center;">Actualice su contraseña!</h2>
						<form class="form-horizontal" id="changepasswd" method="post" action="index.php?view=changepasswd" role="form">
							<input type="password" class="form-control" id="password" name="password" placeholder="Contraseña Actual" />
							<input type="password" class="form-control"  id="newpassword" name="newpassword" placeholder="Nueva Contraseña"/>
							<input type="password" class="form-control" id="confirmnewpassword" name="confirmnewpassword" placeholder="Confirmar Nueva Contraseña"/>
							<button type="submit" class="btn btn-default" style="margin: auto">Cambiar Contraseña</button>
						</form>

						<script>
						$("#changepasswd").submit(function(e){
							if($("#password").val()=="" || $("#newpassword").val()=="" || $("#confirmnewpassword").val()==""){
								e.preventDefault();
								alert("No debes dejar espacios vacios.");

							}else{
								if($("#newpassword").val() == $("#confirmnewpassword").val()){
						//			alert("Correcto");			
								}else{
									e.preventDefault();
									alert("Las nueva contraseña no coincide con la confirmacion.");
								}
							}
						});

						</script>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section><!--/form-->
	
	
		<footer id="footer"><!--Footer-->
		<div class="footer-top">
			<div class="container">
				<div class="row-fluid">
					
					<div class="col-sm-12">
						<div class="col-sm-2">
							<div class="companyinfo">
									<h2><span>RED</span>-IDi</h2>
									<p>Investigación, Desarrollo e Innovación</p>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="img/home/iframe6.jpg" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-link"></i>
									</div>
								</a>
								<p>EDUNI</p>
								<h2>Info Adicional</h2>
							</div>
						</div>
						
						<div class="col-sm-2">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="img/home/iframe6.jpg" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-link"></i>
									</div>
								</a>
								<p>Fondo Editorial PUCP</p>
								<h2>Info Adicional</h2>
							</div>
						</div>
						
						<div class="col-sm-2">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="img/home/iframe6.jpg" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-link"></i>
									</div>
								</a>
								<p>Centro Editorial UPCH</p>
								<h2>Info Adicional</h2>
							</div>
						</div>
						
						<div class="col-sm-2">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="img/home/iframe6.jpg" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-link"></i>
									</div>
								</a>
								<p>Fondo Editorial UNMSM</p>
								<h2>Info Adicional</h2>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="img/home/iframe6.jpg" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-link"></i>
									</div>
								</a>
								<p>Fondo Editorial UNALM</p>
								<h2>Info Adicional</h2>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Universidades</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">UNI</a></li>
								<li><a href="#">PUCP</a></li>
								<li><a href="#">UPCH</a></li>
								<li><a href="#">UNSM</a></li>
								<li><a href="#">UNALM</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Legal</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Terminos de Uso</a></li>
								<li><a href="#">Política de Privacidad</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Información</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Nosotros</a></li>
								<li><a href="#">Puntos de Venta</a></li>
								<li><a href="#">Normas de Publicación</a></li>
								
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Adicional</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Subitem 1</a></li>
								<li><a href="#">Subitem 2</a></li>
								<li><a href="#">Subitem 3</a></li>
								
							</ul>
						</div>
					</div>
					<div class="col-sm-3 col-sm-offset-1">
						<div class="single-widget">
							<h2>Suscribete</h2>
							<form action="#" class="searchform">
								<input type="text" placeholder="Tu email..." />
								<button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
								<p>Se te notificará cuando actualicemos <br />nuestro repositorio de libros...</p>
							</form>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Todos los Derechos Reservados.</p>
					<p class="pull-right">Modificado por <span><a target="_blank" href="http://www.themeum.com">Thanatos</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
	

  
    <script src="js/front/jquery.js"></script>
	<script src="js/front/bootstrap.min.js"></script>
	<script src="js/front/jquery.scrollUp.min.js"></script>
	<script src="js/front/price-range.js"></script>
    <script src="js/front/jquery.prettyPhoto.js"></script>
    <script src="js/front/main.js"></script>
</body>
</html>