<?php
class Database {
	public static $db;
	public static $con;
	function Database(){
		$this->user="root";$this->pass="root";$this->host="localhost";$this->ddbb="library2";
	}

	function connect(){
		$con = new mysqli($this->host,$this->user,$this->pass,$this->ddbb);
		$con->set_charset("utf8");
		/*********
		Descomentar para verificar que se usa codificación utf8
		*********/

		/*if (!mysqli_set_charset($con, "utf8")) {
		    printf("Error cargando el conjunto de caracteres utf8: %s\n", mysqli_error($con));
		} else {
		    printf("Conjunto de caracteres actual: %s\n", mysqli_character_set_name($con));
		}*/
		return $con;
	}

	public static function getCon(){
		if(self::$con==null && self::$db==null){
			self::$db = new Database();
			self::$con = self::$db->connect();
		}
		return self::$con;
	}
	
}
?>
