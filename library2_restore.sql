-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 23-08-2017 a las 09:53:38
-- Versión del servidor: 5.7.19-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `library2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `author`
--

CREATE TABLE `author` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `lastname` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `author`
--

INSERT INTO `author` (`id`, `name`, `lastname`) VALUES
(1, 'Antón', ' Chéjov'),
(2, ' William ', 'Faulkner'),
(3, 'Michel ', 'de Montaign'),
(4, 'Laurence', 'Sterne'),
(5, ' Joseph', 'Conrad '),
(6, 'Robert Louis', 'Stevenson'),
(7, 'Leopoldo', ' Alas Clarín'),
(8, 'Virginia', 'Woolf'),
(9, 'Giorgio', 'Bassani'),
(10, 'Jorge', 'Gracia Ibañez'),
(11, 'Jessica', 'Brockmole'),
(12, 'Shigeru', 'Miyamoto'),
(13, 'Fausto Antonio', 'Ramirez'),
(14, 'William', 'Ospina'),
(15, 'Claudia', 'Marcucetti Pascoli');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `book`
--

CREATE TABLE `book` (
  `id` int(11) NOT NULL,
  `isbn` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `title` varchar(200) CHARACTER SET latin1 NOT NULL,
  `subtitle` varchar(1000) CHARACTER SET latin1 NOT NULL,
  `description` varchar(1000) CHARACTER SET latin1 NOT NULL,
  `file` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `image` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `n_pag` int(11) DEFAULT NULL,
  `price` decimal(6,2) NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `editorial_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `book_review` varchar(500) DEFAULT NULL,
  `table_contents` varchar(1000) DEFAULT NULL,
  `collection` varchar(100) DEFAULT NULL,
  `edition` int(3) DEFAULT NULL,
  `n_pages` int(5) DEFAULT NULL,
  `format` varchar(100) DEFAULT NULL,
  `dimension` varchar(20) DEFAULT NULL,
  `weight` varchar(20) DEFAULT NULL,
  `tag` varchar(100) DEFAULT NULL,
  `languaje` varchar(20) DEFAULT NULL,
  `url_contac` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `book`
--

INSERT INTO `book` (`id`, `isbn`, `title`, `subtitle`, `description`, `file`, `image`, `year`, `n_pag`, `price`, `author_id`, `editorial_id`, `category_id`, `book_review`, `table_contents`, `collection`, `edition`, `n_pages`, `format`, `dimension`, `weight`, `tag`, `languaje`, `url_contac`) VALUES
(1, '123421421', 'Algebra Lineal I', 'Libro de Algebra Lineal ', 'Libro de Algebra Lineal ', NULL, NULL, 2012, 400, '0.00', 8, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '4452315423', 'Arqueologia en el Peru', 'Arqueologia en el Peru', 'Arqueologia en el Peru', NULL, NULL, 1998, 500, '0.00', 7, 4, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '4561616', 'El maltrato familiar hacia personas mayores', 'Un analisis sociojuridico', 'El maltrato familiar hacia las personas mayores constituye una realidad oculta con escasa visibilidad social, académica y mediática. Nuestras sociedades, cada vez más envejecidas, sin embargo, discriminan a las personas mayores ...', NULL, 'img/home/libro1.jpg', NULL, NULL, '56.00', 10, 2, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '65651651651', 'Cartas desde la Isla de Skye', '', 'Marzo de 1912. Elspeth Dunn —una joven escritora que jamás ha abandonado su hogar en la remota isla escocesa de Skye— recibe la carta de un admirador norteamericano. La firma el universitario David Graham y supone para ella una puerta al mundo...', NULL, 'img/home/libro2.jpg', NULL, NULL, '72.00', 11, 3, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '6161515651', 'The Legend of Zelda', 'Hyrule Historia', 'The Legend of Zelda: Hyrule Historia es un libro creado por Nintendo sobre la historia de la serie de videojuegos The Legend of Zelda, siendo la primera publicación donde se da a conocer de manera ...', NULL, 'img/home/libro3.jpg', NULL, NULL, '120.00', 12, 2, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '655651156651', 'El silencio del almendro', '', 'La muerte de Madeleine Limoges, una afamada cantante de ópera, es el detonante de la narración retrospectiva que hace un hombre abatido por el dolor y la soledad. A través de sus recuerdos, desde la infancia hasta la culminación en el amor...', NULL, 'img/home/libro4.jpg', NULL, NULL, '10.00', 13, 4, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '616561515665165', 'Las auroras de sangre', 'Juan de Castellanos y el descubrimiento poetico de America', 'En este extenso ensayo, William Ospina, analiza de manera magistral al poeta Juan de Castellanos y su obra sobre la conquista de Ámerica. La suerte de las Elegías de varones ilustres de Indias de Juan de Castellanos ha sido desgraciada e injusta. Ni en la época de su nacimiento ni después, ni en España ni en América, la obra ha sido...', NULL, 'img/home/libro5.jpg', NULL, NULL, '159.00', 14, 1, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '944484899894', 'De Lecturas y Vidas', '80 entrevistas sobre el poder de los libros', 'Detrás de un gran personaje hay un gran libro. Ésta es la premisa de Claudia Marcucetti quien, a partir de su propia experiencia, indaga en las lecturas y vidas de ochenta personalidades. El resultado es un testimonio extraordinario, lleno de anécdotas y reflexiones conmovedoras que atestiguan, una y otra vez, las profundas transformaciones que leer trae consigo. ...', NULL, 'img/home/libro6.jpg', NULL, NULL, '240.00', 15, 2, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Algebra Lineal'),
(2, 'Biologia Computacional'),
(3, 'Simulacion'),
(4, 'Inteligencia Artificial'),
(5, 'Actualidad'),
(6, 'Antropología'),
(7, 'Arqueología'),
(8, 'Arquitectura'),
(9, 'Arte'),
(10, 'Ciencias Sociales'),
(11, 'Ciencia Política'),
(12, 'Comunicación'),
(13, 'Derecho'),
(14, 'Economía'),
(15, 'Filosofía'),
(16, 'Gestión'),
(17, 'Historia'),
(18, 'Ingeniería'),
(19, 'Investigación'),
(20, 'Lenguas Peruanas'),
(21, 'Linguística'),
(22, 'Literatura'),
(23, 'Matemática'),
(24, 'Música'),
(25, 'Psicología'),
(26, 'Química');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `lastname` varchar(50) CHARACTER SET latin1 NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `address` varchar(60) CHARACTER SET latin1 NOT NULL,
  `phone` varchar(60) CHARACTER SET latin1 NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `client`
--

INSERT INTO `client` (`id`, `name`, `lastname`, `email`, `address`, `phone`, `is_active`, `created_at`) VALUES
(1, 'Libreria Anita', ' ', 'lanita@librerias.pe', ' Av. Arequipa 810, Piso 8,. Cercado de Lima, Lima - PerÃº', '415415152', 1, '2017-07-11 13:52:47'),
(2, 'Libreria Pablo', 'Escobar', 'pescobar@librerias.pe', ' Calle Schell 335. A media cuadra de Larco', '54113215215', 1, '2017-07-11 13:55:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `editorial`
--

CREATE TABLE `editorial` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET latin1 NOT NULL,
  `mail` varchar(100) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `editorial`
--

INSERT INTO `editorial` (`id`, `name`, `mail`) VALUES
(1, 'Fondo Editorial UNMSM', 'edunmmsm@unmmsm.pe'),
(2, 'EDUNI', 'eduni@uni.pe'),
(3, 'Fondo Editorial UNALM', 'edunalm@unalm.pe'),
(4, 'Fondo Editorial PUCP', 'edpucp@pucp.pe'),
(5, 'Centro Editorial - UPCH', 'edupch@upch.pe');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `code` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `item`
--

INSERT INTO `item` (`id`, `code`, `status_id`, `book_id`) VALUES
(1, '424424356', 2, 1),
(2, '234532523', 1, 1),
(3, '575473467', 1, 1),
(4, '452353265', 1, 2),
(5, '684573734', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operation`
--

CREATE TABLE `operation` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `start_at` date NOT NULL,
  `finish_at` date NOT NULL,
  `returned_at` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `receptor_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `operation`
--

INSERT INTO `operation` (`id`, `item_id`, `client_id`, `start_at`, `finish_at`, `returned_at`, `user_id`, `receptor_id`) VALUES
(1, 1, 2, '2017-07-11', '2017-07-12', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `status`
--

INSERT INTO `status` (`id`, `name`) VALUES
(1, 'Disponible'),
(2, 'Ocupado'),
(3, 'Inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `lastname` varchar(50) CHARACTER SET latin1 NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` varchar(60) CHARACTER SET latin1 NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `name`, `lastname`, `username`, `email`, `password`, `is_active`, `is_admin`, `created_at`) VALUES
(1, 'Administrador', '', 'admin', '', '90b9aa7e25f80cf4f64e990b78a9fc5ebd6cecad', 1, 1, '2017-07-11 10:54:45'),
(2, 'Erick', 'Lazon', 'elv', '', 'd0206740edb834ab7c24b19f65056ade622a20f7', 1, 0, '2017-07-11 11:03:48'),
(3, 'Julito', 'Cesar', 'julito', '', '4e54c84c8785e37db7f22f56efd404d3ee31c009', 1, 0, '2017-07-11 13:51:34');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author_id` (`author_id`),
  ADD KEY `editorial_id` (`editorial_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indices de la tabla `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `editorial`
--
ALTER TABLE `editorial`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `book_id` (`book_id`);

--
-- Indices de la tabla `operation`
--
ALTER TABLE `operation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `receptor_id` (`receptor_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indices de la tabla `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `author`
--
ALTER TABLE `author`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `book`
--
ALTER TABLE `book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `editorial`
--
ALTER TABLE `editorial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `operation`
--
ALTER TABLE `operation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `book`
--
ALTER TABLE `book`
  ADD CONSTRAINT `book_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `author` (`id`),
  ADD CONSTRAINT `book_ibfk_2` FOREIGN KEY (`editorial_id`) REFERENCES `editorial` (`id`),
  ADD CONSTRAINT `book_ibfk_3` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Filtros para la tabla `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `item_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`),
  ADD CONSTRAINT `item_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`);

--
-- Filtros para la tabla `operation`
--
ALTER TABLE `operation`
  ADD CONSTRAINT `operation_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  ADD CONSTRAINT `operation_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `operation_ibfk_3` FOREIGN KEY (`receptor_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `operation_ibfk_4` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
